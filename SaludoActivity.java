package com.cip.pdm.saludo;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class SaludoActivity extends AppCompatActivity {

    private Button bt_saludo;
    private TextView txtview_saludo;
    private EditText text_nombre;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_saludo);

        bt_saludo= (Button) findViewById(R.id.bt_saludo);
        txtview_saludo = (TextView) findViewById(R.id.txtview_saludo);
        text_nombre = (EditText) findViewById(R.id.text_nombre);

        bt_saludo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                txtview_saludo.setText("Bienvenido "+text_nombre.getText());
            }
        });
    }


}
